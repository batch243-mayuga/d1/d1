// console.log("TGIF!");

// What are conditional statements?

// Conditional statements allow us to control the flow of our program.
// It also allows us to run statement/instruction if a condition is met or run another separate instruction if otherwise.

// [Section] if, else if, else Statement

let numA = -1;

/*
	if statement
	- it will execute the statement if the specified condition is met or true.
*/

if(numA<0){
	console.log("Hello");
}

//  to check if true - console.log(numA<0);

/*
	Syntax:
	if(condition){
		statement;
	}
*/
// The result of the expression added in the if's condtion must result to true, else, the statement inside if() will not run.

// lets update the variable and run an if statement with the same condition. 

numA = 0

if(numA<0){
	console.log("Hello again if numA is 0!")
}


// It will not run because the expression now results to false.

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York!");
}

// else if clause
/*
	-Executes a statement if previous conditions are false and if the specified condition is true.
	-The else if clause is optional and can be added to capture additional conditions to change the flow of a program.
*/

let numH = 1;

if (numH < 0) {
	console.log("Hello from NumH!");
}
else if(numH > 0){
	console.log("Hi I'am numH!");
}

// we were able to run the else if () statements after we evaluated that the if condition was failed/false.

// If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

if (numH>0){
	console.log("Hello from NumH!");
}
else if(num === 1){
	console.log("Hi Im the second condition met!");
}
else if(numH<0){
	console.log("Hi I'm numH!");
}

console.log(numH>0);
console.log(numH ===1);
console.log(numH<0);

// else if () statement was not executed because the if statement was able to run, the evaluation of the whole statement stops there. 

// Let's update the city variable and look at the another example

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York! ");
}
else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}


// else Statement
/*
	-Executes a statement if all other condition are false/ not met
	-else statement is optional and can be added to capture any other result to change the flow of the program.
*/

/*numH = 2;

if(numH<0){
	console.log("Hello I'm numH");
}
else if(numH>2);{
	console.log("numH is greater than 2");
}
else if(numH>3){
	console.log("numH is greater than 3");
}
else{
	console.log("numH from else");
}*/

/*
	since all of the preceding if and else if condition failed, the else statement was run instead.

	Else and else if statements should only be added if there is a preceeding if condition, else, statements by itself will not work, however, if statements will work even if there is no else statement.
*/

	/*
	else{
		console.log("Will not run ");
	}

	It will result into an error.
*/

/*{
	let numB = 1;
	else if(numB ===1){
		console.log("numB===1");
	}

	Same goes for else if, there should be a preceeding if(.)
}*/

	// if, else and else statement with functions

	/*
		Most of the times we would like to use if, else if and else statements with functions to control the flow of our application.
		-By including them inside the functions, we can decide when certain conditions will be checked instead of executing statements when the JavaScript loads
		-The "return" statement can utilized with conditional statements in combination w/ functions to change values to be used for other features of our application.
 	*/

 	let message;

 	function determineTyphoonIntensity(windSpeed){
 		if(windSpeed<0){
 			return "Invalid argument";
 		}
 		else if(windSpeed > 0 && windSpeed<31){
 			return "Not a typhoon yet.";
 		}
 		else if(windSpeed <= 60){
 			return "Tropical depression detectted.";
 		}
 		else if(windSpeed>=61 && windSpeed <= 88){
 			return "Tropical Storm detected.";
 		}
 		else if(windSpeed>=89 && windSpeed <= 117){
 			return "Sever Tropical Storm detected.";
 		}
 		else{
 			return "typhoon detected.";
 		}

 	}
 	// Returns the string to the variable message that invoked.
 	message = determineTyphoonIntensity(123);

 	console.log(message);

 	/*
		-We can further control the flow of our program based on conditions and changing variables and results.
		-Due to the conditional statements created in the situation, we were able to reassign its value and its new value to print differenet output
		-console.warn() is a good way to print warnings in our console that could help us developers act on certain output w/in the code.
 	*/

 	if(message === "Typhoon detected."){
 		console.warn(message);
 	}

// [Section] Truthy and Falsy.

	/*
		In javascript a "truthy" value is a value that is considered true when encounted in a Boolean context
		Values are considered true unless defined otherwise.
		-Falsy values/ exceptions for truthy:
		1. false
        2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN - Not a Number
	*/

// Truthy Examples

if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}

// Falsy Examples

if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}

if(undefined){
	console.log("Falsy");

}

// [Section] Conditional (Ternary) Operator

/*
	-The conditional (ternary) Operator
	1. condition
	2. expression to execute if the condition is truthy
	3. expression if the condition is falsy

	-can be used as an alternative to an "if else" statement
	-Ternary operators have an implicit "return" statement meaning without return keyword, the resulting expressions can be stored in a variable.
	-Commonly used for single statement execution where the result consist of only one line of code

	-Syntax:
	(expression) ? ifTrue : ifFalse;
*/

// single statement execution
let ternaryResult = (1<18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple statement execution
// Both function perform two separate task which changes the value of the "name" variable and returns the result storing in it in the "legalAge" variable.

let name;

function isOfLegalAge(){
	name = 'John';
	return "You are of the legal age!";
}

function isUnderAge(){
	name = 'Jane';
	return "You are under age limit";
}

// The parseInt() function converts the input receive into a number data type.
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = ( age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of ternary Operator in Function: " + legalAge + ", " + name);

// [Section] Switch Statement
	/*
		The switch statement evaluates an expression and matches the expression's value to a case clause.
	*/

	/*
	Syntax:
	switch (expression) {
		case value:
			statement;
			break;
		default:
			statement;
			break;
	}
	*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
	case 'monday':
		console.log("The color of the day is red!");
		break;
	case 'tuesday':
		console.log("The color of the day is orange!");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow!");
		break;
	case 'thursday':
		console.log("The color of the day is green!");
		break;
	case 'friday':
		console.log("The color of the day is blue!");
		break;
	case 'saturday':
		console.log("The color of the day is indigo!");
		break;
	case 'sunday':
		console.log("The color of the day is violet!");
		break;
	default:
		console.log("Pleas input a valid day.");
		break;
}

// [Section] Try-catch-finally Statement
	//"try catch" statement are commonly use for error handling.
	// There are instances when the application returns an error/warning that is not necessarily an error in the context of our codes.
	// These errors are result of an attempt of the programming language to help developers in creating efficient code.
	// They are used to specify a response whenever an exception/error is received.

	
		try{
			alerat(determineTyphoonIntensity(windSpeed));
		}
		catch(error){
			console.warn(error.message);
		}
		finally{
			alert("Intensity updates will show alert");
		}
	}

	showIntensityAlert(110);